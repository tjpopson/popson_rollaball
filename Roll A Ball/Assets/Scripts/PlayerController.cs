﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    private Rigidbody rb;
    public float speed;
    private int count;
    public Text countText;
    public Text winText;
    public Text timerText;
    public float timelimit;
    private int Boost;
    private float Boosttime;
    public int jumpForce;
    private bool win = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        Boost = 1;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (Boost > 1) {

            Boosttime -= Time.deltaTime;
        
        }

        if (Boosttime<= 0){

            Boost = 1;

        }

        if (timelimit > 0 && !win)
        {
            timelimit -= Time.deltaTime;
        }
        else if(timelimit <= 0 && !win){
            winText.text = "You Lose";
        }
        timerText.text = timelimit.ToString();
    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed * Boost);

        if (rb.position.y <= .5) {
            if (Input.GetKeyDown("space")) {

                rb.AddForce(new Vector3(0, 1, 0) * jumpForce);

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }

        if (other.gameObject.CompareTag("Power Up"))
        {
            Boost = 2;
            Boosttime = 1;
            other.gameObject.SetActive(false);
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
            win = true;
        }
    }
}
